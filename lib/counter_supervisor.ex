defmodule Counter.Supervisor do
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, 0)
  end

  @impl true
 def init(_) do
   children = [
     {Counter, 0}
   ]

   Supervisor.init(children, strategy: :one_for_one)
 end
end
