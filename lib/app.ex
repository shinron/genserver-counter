defmodule CounterApp do
  use Application

  def start(_type, _args) do
    Counter.Supervisor.start_link
  end
end
