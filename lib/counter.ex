defmodule Counter do
  use GenServer

  # Client

  def start_link(_) do
    GenServer.start_link(__MODULE__, 0, name: :counter)
  end

  def show do
    GenServer.call(:counter, :show)
  end

  def increment do
    GenServer.cast(:counter, :increment)
  end

  def decrement do
    GenServer.cast(:counter, :decrement)
  end

  def reset do
    GenServer.cast(:counter, :reset)
  end

  # Server (callbacks)

  @impl true
  def init(_) do
    {:ok, 0}
  end

  @impl true
  def handle_call(:show, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_cast(:increment, state) do
    state = state + 1
    {:noreply, state}
  end

  @impl true
  def handle_cast(:reset, state) do
    state = 0
    {:noreply, state}
  end

  @impl true
  def handle_cast(:decrement, state) do
    state = state - 1
    {:noreply, state}
  end
end
