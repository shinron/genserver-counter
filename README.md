# Problem Statement
## Create a Mix Counter App with following features
- It should persist counter state in memory
- It should allow incrementing counter by 1
- It should allow decrementing counter by 1
- It can also support negative value
- It should allow reseting counter
- It should allow to know current counter value

## Tech Criteria
- Use Genserver
- On killing Genserver process it should restart automatically by reseting counter
- Example Operations
> Counter.show()
  - 0
> Counter.increment()
  - 1
> Counter.decrement()
  - 0
> Counter.show()
  - 0

## Instructions
- Clone the repo.
- Execute the command `iex -S mix`.
